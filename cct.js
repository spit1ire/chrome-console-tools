function downloadObjectAsJson(exportObj, exportName){
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", exportName + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
}

function search (reg, startNode = document.body) {
  const list = startNode.childNodes;
  const result = [];
  
	function traverse( node ) {
    if (node && node.nodeName === '#text' && node.nodeValue.match(reg)) {
    //   console.dir(node);
      result.push(node);
    }
		const children = node.childNodes;

		for ( let i = 0, l = children.length; i < l; i ++ ) {
			traverse(children[ i ]);
		}
	}
  
  list.forEach(n=>traverse(n));
  console.log(result);
}


const script = document.createElement('script');
const code   = document.createTextNode(`
    window.cct = {
        downloadObjectAsJson: ${downloadObjectAsJson},
        search: ${search}
    }
`);
script.appendChild(code);
(document.body || document.head || document.documentElement).appendChild(script);
